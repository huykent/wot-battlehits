﻿
[![Quality Gate Status](https://sonarcloud.io/api/project_badges/measure?project=P0LIR0ID_wot-battlehits&metric=alert_status)](https://sonarcloud.io/dashboard?id=P0LIR0ID_wot-battlehits)

**BattleHits** This is a modification for the game "World Of Tanks" which allows you to show shots dealt by you or to your vehicle in lobby

Code license: CC BY-NC-SA 4.0

P0LIR0ID 2015-2020

### An example of what a mod looks
![An example of what a mod looks](https://static.poliroid.ru/battleHits.jpg)

